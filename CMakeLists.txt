cmake_minimum_required(VERSION 2.8)
project(maya-plugin-boilerplate)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules)

# set Maya Devkit location
set(MAYA_INCLUDE_DIR "/opt/devkitBase/include")

add_subdirectory(src)