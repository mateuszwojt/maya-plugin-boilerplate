# download and extract Maya 2022
wget -q -P /tmp/ https://efulfillment.autodesk.com/NetSWDLD/2022/MAYA/7AA0A333-D72A-3C65-AB0F-0FE0F802014A/ESD/Autodesk_Maya_2022_ML_Linux_64bit.tgz
tar -C /tmp -xvf /tmp/Autodesk_Maya_2022_ML_Linux_64bit.tgz
cd /tmp
rpm -Uvh /tmp/Packages/Maya*.rpm
rpm -Uvh /tmp/Packages/Bifrost*.rpm
rpm -Uvh /tmp/Packages/Pymel*.rpm

# download and extract Maya Devkit
wget -q -P /tmp/ https://autodesk-adn-transfer.s3-us-west-2.amazonaws.com/ADN+Extranet/M%26E/Maya/devkit+2022/Autodesk_Maya_2022_DEVKIT_Linux.tgz
tar -C /tmp -xvf /tmp/Autodesk_Maya_2022_DEVKIT_Linux.tgz
mv -v devkitBase /opt/

# cleanup /tmp
rm -vf /tmp/*


# build plugin
cd $CI_PROJECT_DIR
mkdir build && cd build
cmake ..
make
make install
cd ..